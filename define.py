import requests
import json

def getCreds():
    creds = dict()
    creds['acces_token'] = 'ACCES_TOKEN_HERE'
    creds['instagram_account_id'] = 'INSTA_ID_HERE'
    creds['graph_domain'] = 'https://graph.facebook.com'
    creds['graph_version'] = 'v18.0'
    creds['endpoint_base'] = creds['graph_domain'] + creds['graph_version'] + '/'    
    
    return creds


def makeAPICALL(url, endpointParams, type) :
    
    if type == 'POST' :
        data = requests.post( url, endpointParams )
    else : 
        data = requests.get( url, endpointParams )

    response = dict()
    response['url'] = url
    response['endpoint_params'] = endpointParams
    response['endpoint_params_pretty'] = json.dump( endpointParams, indent = 4 )
    response['json_data'] = json.load( data.content )
    response['json_data_pretty'] = json.dump( response['json_data'], indent = 4 )
