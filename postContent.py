""" TODO: 
    > make access to shared folder on google
    > post every hour loop
        > create caption
        > use pictures of last hour
"""



import time
from define import getCreds, makeAPICALL

params = getCreds()
params['media_type'] = 'IMAGE'
params['media_url'] = 'MEDIA_URL_HERE'
params['caption'] = 'dailyDiary_xxxx yy:yy'
params['caption'] += '\n [INSERT TEXT HERE]'


def createMediaObject( params ) :
    url = params['endpoint_base'] + params['instagram_account_id'] + '/media'

    endpointParams = dict()
    endpointParams['caption'] = params['caption']
    endpointParams['access_token'] = params['access_token']

    if params['media_type'] == 'IMAGE' : 
        endpointParams['image_url'] = params['media_url']
    else : 
        endpointParams['video_url'] = params['media_url']
        endpointParams['media_type'] = params['media_type']

    return makeAPICALL( url, endpointParams, 'POST')


def getMediaObjectStatus ( mediaObjectId, params ) :
    url = params['endpoint_base'] + params['instagram_account_id'] + '/' + mediaObjectId 

    endpointParams = dict()
    endpointParams['fields'] = 'status_code'
    endpointParams['access_token'] = params['access_token']

    return makeAPICALL( url, endpointParams, 'GET')

def publishMedia ( mediaObjectId, params) :
    url = params['endpoint_base'] + params['instagram_account_id'] + '/' + mediaObjectId 

    endPointParams = dict()
    endPointParams['creation_id'] = mediaObjectId
    endPointParams['access_token'] = params['acces_token']

    return makeAPICALL( url, endPointParams, 'POST')


imageMediaObjectResponse = createMediaObject( params )
imageMediaObjectId = imageMediaObjectResponse['json_data']['id']
imageMediaObjectStatusCode = 'IN PROGRESS'

print( "\n --- IMAGE MEDIA OBJECT --- \n" )
print( "\tID:" )
print( "\t" + imageMediaObjectId )

while imageMediaObjectStatusCode != 'FINISHED' :
    imageMediaObjectStatusResponse = getMediaObjectStatus( imageMediaObjectId, params )
    imageMediaStatusCode = imageMediaObjectStatusResponse['json_data']['status_code']

    print( "\n --- IMAGE MEDIA OBJECT STATUS--- \n" )
    print( "\tSTATUS CODE:" )
    print( "\t" + imageMediaStatusCode )

    time.sleep( 10 )

publishImageResponse = publishMedia ( imageMediaObjectId, params)

print( "\n --- PUBLISHED IMAGE RESPONSE --- \n" )
print( "\tRESPONSE:" )
print( "\t" + publishImageResponse['json_data_pretty'] )
